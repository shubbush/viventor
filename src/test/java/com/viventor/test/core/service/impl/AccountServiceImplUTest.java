package com.viventor.test.core.service.impl;

import com.viventor.test.core.domain.Account;
import com.viventor.test.core.event.TransactionEvent;
import com.viventor.test.core.exception.NotEnoughFundsException;
import com.viventor.test.core.repository.AccountRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationEventPublisher;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

/**
 * @author shubanev.a
 */
public class AccountServiceImplUTest {

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private ApplicationEventPublisher publisher;

    private AccountServiceImpl accountService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        when(accountRepository.findOne(anyLong()))
                .thenReturn(Account.builder().balance(new BigDecimal(5.00)).build());

        when(accountRepository.save(any(Account.class))).thenAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            return args[0];
        });

        accountService = new AccountServiceImpl(accountRepository, publisher);
    }

    @Test(expected = NotEnoughFundsException.class)
    public void withdrawOverdraft() throws Exception {
        accountService.withdraw(1L, new BigDecimal(6.00));
    }

    @Test
    public void withdraw() throws Exception {
        Account account = accountService.withdraw(1L, new BigDecimal(3.00));
        assertEquals(new BigDecimal(2.0), account.getBalance());
        verify(publisher, times(1)).publishEvent(any(TransactionEvent.class));
    }

    @Test
    public void deposit() throws Exception {
        Account account = accountService.deposit(1L, new BigDecimal(6.00));
        assertEquals(new BigDecimal(11.0), account.getBalance());
        verify(publisher, times(1)).publishEvent(any(TransactionEvent.class));
    }

}