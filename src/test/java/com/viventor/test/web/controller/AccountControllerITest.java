package com.viventor.test.web.controller;

import com.viventor.test.base.AbstractWebITest;
import com.viventor.test.base.DBInit;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author shubanev.a
 */
public class AccountControllerITest extends AbstractWebITest {

    @Autowired
    private DBInit dbInit;

    @Test
    public void balance() throws Exception {
        mockMvc.perform(get("/api/account/" + dbInit.getAccountId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("balance").value(10.0));
    }

    @Test
    public void withdraw() throws Exception {
        mockMvc.perform(get("/api/account/" + dbInit.getAccountId() + "/withdraw")
                .param("amount", "4.0"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("balance").value(6.0));
    }

    @Test
    public void deposit() throws Exception {
        mockMvc.perform(get("/api/account/" + dbInit.getAccountId() + "/deposit")
                .param("amount", "4.0"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("balance").value(14.0));
    }
}
