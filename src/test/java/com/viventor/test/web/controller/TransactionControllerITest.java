package com.viventor.test.web.controller;

import com.viventor.test.base.AbstractWebITest;
import com.viventor.test.base.DBInit;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author shubanev.a
 */
public class TransactionControllerITest extends AbstractWebITest {

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

    @Autowired
    private DBInit dbInit;

    @Test
    public void statement() throws Exception {

        LocalDateTime from = LocalDateTime.now(Clock.systemUTC()).with(LocalTime.MIN);
        LocalDateTime to = LocalDateTime.now(Clock.systemUTC()).with(LocalTime.MAX);

        mockMvc.perform(get("/api/account/" + dbInit.getAccountId() + "/withdraw")
                .param("amount", "4.0"))
                .andExpect(status().isOk());


        await().atMost(5, SECONDS).until(() ->
                objectMapper.readValue(mockMvc.perform(get("/api/transaction/" + dbInit.getClientId())
                        .param("from", formatter.format(from))
                        .param("to", formatter.format(to)))
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse()
                        .getContentAsByteArray(), List.class).size() == 1);

    }

}
