package com.viventor.test.web.controller;

import com.viventor.test.base.AbstractWebITest;
import com.viventor.test.web.dto.RegistrationDto;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author shubanev.a
 */
public class ClientControllerITest extends AbstractWebITest {

    @Test
    public void register() throws Exception {
        String email = "test1@test.com";
        RegistrationDto dto = RegistrationDto.builder()
                .email(email)
                .password("password")
                .build();

        MvcResult result = mockMvc.perform(post("/api/client/register")
                .content(objectMapper.writeValueAsBytes(dto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        String location = result.getResponse().getHeader("Location");
        mockMvc.perform(get(location))
                .andExpect(status().isOk())
                .andExpect(jsonPath("email").value(email))
                .andExpect(jsonPath("account.balance").value(0.00));
    }

}