package com.viventor.test.base;

import com.viventor.test.core.domain.Account;
import com.viventor.test.core.domain.Client;
import com.viventor.test.core.repository.ClientRepository;
import com.viventor.test.core.repository.TransactionRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * @author shubanev.a
 */
@Component
@Getter
public class DBInit {

    private Long clientId;
    private Long accountId;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    public void initDocuments() {
        initClients();
    }

    private void initClients() {
        Account account = Account.builder()
                .balance(new BigDecimal(10.0))
                .build();

        Client client = Client
                .builder()
                .email("test@test.com")
                .password("password")
                .account(account)
                .build();

        Client savedClient = clientRepository.save(client);
        clientId = savedClient.getId();
        accountId = savedClient.getAccount().getId();
    }

    public void clear() {
        clientRepository.deleteAll();
    }
}
