package com.viventor.test.core.event;

import com.viventor.test.core.domain.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

/**
 * @author shubanev.a
 */
@Getter
@AllArgsConstructor
public class TransactionEvent {

    private Long account;
    private BigDecimal amount;
    private BigDecimal balance;
    private TransactionType type;

}
