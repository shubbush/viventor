package com.viventor.test.core.repository;

import com.viventor.test.core.domain.Client;
import org.springframework.data.repository.CrudRepository;

/**
 * @author shubanev.a
 */
public interface ClientRepository extends CrudRepository<Client, Long> {

    Client findByEmail(String email);
}
