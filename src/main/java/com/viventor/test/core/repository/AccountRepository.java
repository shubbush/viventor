package com.viventor.test.core.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.viventor.test.core.domain.Account;

/**
 * @author shubanev.a
 */
public interface AccountRepository extends PagingAndSortingRepository<Account, Long> {

}
