package com.viventor.test.core.repository;

import com.viventor.test.core.domain.Transaction;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author shubanev.a
 */
public interface TransactionRepository extends PagingAndSortingRepository<Transaction, Long> {

    List<Transaction> findByAccountClientIdAndDateTimeBetween(Long id, LocalDateTime from, LocalDateTime to);
}
