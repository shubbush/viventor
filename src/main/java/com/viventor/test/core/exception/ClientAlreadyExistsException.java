package com.viventor.test.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author shubanev.a
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ClientAlreadyExistsException extends RuntimeException {

    public ClientAlreadyExistsException() {
    }

    public ClientAlreadyExistsException(String message) {
        super(message);
    }
}
