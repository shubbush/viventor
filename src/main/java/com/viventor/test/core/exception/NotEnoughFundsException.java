package com.viventor.test.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author shubanev.a
 */
@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class NotEnoughFundsException extends RuntimeException {

    public NotEnoughFundsException() {
    }

    public NotEnoughFundsException(String message) {
        super(message);
    }
}
