package com.viventor.test.core.domain;

/**
 * @author shubanev.a
 */
public enum TransactionType {

    WITHDRAW, DEPOSIT;
}
