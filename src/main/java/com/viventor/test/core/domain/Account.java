package com.viventor.test.core.domain;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;

/**
 * @author shubanev.a
 */
@Table(name = "account")
@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
public class Account {

    private static final Currency EUR = Currency.getInstance("EUR");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(mappedBy = "account")
    private Client client;

    @Column(name = "balance")
    private BigDecimal balance;

    @Builder.Default
    @Column(name = "currency")
    private Currency currency = EUR;

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
    private List<Transaction> transactions;

    @Version
    @Column(name = "version", nullable = false)
    private Long version;

    public Account() {
        this.currency = EUR;
        this.balance = BigDecimal.valueOf(0.0);
    }

    @Transient
    public void deposit(BigDecimal amount) {
        balance = balance.add(amount);
    }

    @Transient
    public void withdraw(BigDecimal amount) {
        balance = balance.subtract(amount);
    }
}
