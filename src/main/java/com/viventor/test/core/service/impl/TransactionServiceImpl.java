package com.viventor.test.core.service.impl;

import com.viventor.test.core.domain.Account;
import com.viventor.test.core.domain.Transaction;
import com.viventor.test.core.event.TransactionEvent;
import com.viventor.test.core.repository.AccountRepository;
import com.viventor.test.core.repository.TransactionRepository;
import com.viventor.test.core.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author shubanev.a
 */
@Service
@Transactional(readOnly = true)
public class TransactionServiceImpl implements TransactionService {

    private TransactionRepository transactionRepository;
    private AccountRepository accountRepository;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository,
                                  AccountRepository accountRepository) {
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
    }

    @Async
    @TransactionalEventListener(classes = TransactionEvent.class)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void handleTransactionEvent(TransactionEvent event) {
        Account account = accountRepository.findOne(event.getAccount());

        Transaction transaction = Transaction.builder()
                .amount(event.getAmount())
                .balance(event.getBalance())
                .type(event.getType())
                .account(account)
                .dateTime(LocalDateTime.now(Clock.systemUTC()))
                .build();

        transactionRepository.save(transaction);
    }

    @Override
    public List<Transaction> statement(Long clientId, LocalDateTime from, LocalDateTime to) {
        return transactionRepository.findByAccountClientIdAndDateTimeBetween(clientId, from, to);
    }

}
