package com.viventor.test.core.service.impl;

import com.viventor.test.core.domain.Account;
import com.viventor.test.core.domain.TransactionType;
import com.viventor.test.core.event.TransactionEvent;
import com.viventor.test.core.exception.EntityNotFoundException;
import com.viventor.test.core.exception.NotEnoughFundsException;
import com.viventor.test.core.repository.AccountRepository;
import com.viventor.test.core.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * @author shubanev.a
 */
@Service
@Transactional(readOnly = true)
public class AccountServiceImpl implements AccountService {

    private AccountRepository accountRepository;
    private ApplicationEventPublisher publisher;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository,
                              ApplicationEventPublisher publisher) {
        this.accountRepository = accountRepository;
        this.publisher = publisher;
    }

    @Override
    public Account find(Long id) {
        Account account = accountRepository.findOne(id);
        if (account == null)
            throw new EntityNotFoundException(String.format("Account with id %d not found", id));

        // Should check that the authorized user is an account holder

        return account;
    }

    @Transactional
    @Override
    public Account withdraw(Long id, BigDecimal amount) {
        Account account = find(id);
        if (!hasEnoughFunds(account, amount))
            throw new NotEnoughFundsException(String.format("Not enough fund at the account %d", id));

        account.withdraw(amount);

        publishTransactionEvent(id, amount, account.getBalance(), TransactionType.WITHDRAW);
        return accountRepository.save(account);
    }

    @Transactional
    @Override
    public Account deposit(Long id, BigDecimal amount) {
        Account account = find(id);
        account.deposit(amount);

        publishTransactionEvent(id, amount, account.getBalance(), TransactionType.DEPOSIT);
        return accountRepository.save(account);
    }

    private boolean hasEnoughFunds(Account account, BigDecimal amount) {
        return account.getBalance().compareTo(amount) >= 0;
    }

    private void publishTransactionEvent(Long account, BigDecimal amount,
                                         BigDecimal balance, TransactionType type) {
        publisher.publishEvent(new TransactionEvent(account, amount, balance, type));
    }
}
