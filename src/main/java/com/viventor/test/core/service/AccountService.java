package com.viventor.test.core.service;

import com.viventor.test.core.domain.Account;

import java.math.BigDecimal;

/**
 * @author shubanev.a
 */
public interface AccountService {

    Account find(Long id);

    Account withdraw(Long id, BigDecimal amount);

    Account deposit(Long id, BigDecimal amount);

}
