package com.viventor.test.core.service;

import com.viventor.test.core.domain.Client;


/**
 * @author shubanev.a
 */
public interface ClientService {

    Client create(Client client);

    Client find(Long id);

}
