package com.viventor.test.core.service.impl;

import com.viventor.test.core.domain.Account;
import com.viventor.test.core.domain.Client;
import com.viventor.test.core.exception.ClientAlreadyExistsException;
import com.viventor.test.core.exception.EntityNotFoundException;
import com.viventor.test.core.repository.ClientRepository;
import com.viventor.test.core.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author shubanev.a
 */
@Service
@Transactional(readOnly = true)
public class ClientServiceImpl implements ClientService {

    private ClientRepository clientRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository,
                             PasswordEncoder passwordEncoder) {
        this.clientRepository = clientRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional
    public Client create(Client client) {
        Client existedClient = clientRepository.findByEmail(client.getEmail());
        if (existedClient != null)
            throw new ClientAlreadyExistsException(
                    String.format("Client with email %s already exists", client.getEmail()));

        client.setAccount(new Account());
        client.setPassword(passwordEncoder.encode(client.getPassword()));
        return clientRepository.save(client);
    }

    @Override
    public Client find(Long id) {
        Client client = clientRepository.findOne(id);
        if (client == null)
            throw new EntityNotFoundException(String.format("Client with id %d not found!", id));

        return client;
    }

}
