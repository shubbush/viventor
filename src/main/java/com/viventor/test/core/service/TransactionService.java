package com.viventor.test.core.service;

import com.viventor.test.core.domain.Transaction;
import com.viventor.test.core.event.TransactionEvent;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author shubanev.a
 */
public interface TransactionService {

    void handleTransactionEvent(TransactionEvent event);

    List<Transaction> statement(Long clientId,
                                LocalDateTime from,
                                LocalDateTime to);
}
