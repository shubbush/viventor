package com.viventor.test.web.dto;

import lombok.*;
import org.hibernate.validator.constraints.Email;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author shubanev.a
 */
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientDto {

    private Long id;

    @Email
    @NotNull
    private String email;

    @Valid
    private AccountDto account;

}
