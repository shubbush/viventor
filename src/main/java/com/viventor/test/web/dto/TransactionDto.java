package com.viventor.test.web.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.viventor.test.core.domain.TransactionType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author shubanev.a
 */
@Getter
@Setter
@NoArgsConstructor
public class TransactionDto {

    private Long id;
    private Long accountId;
    private BigDecimal amount;
    private BigDecimal balance;
    private TransactionType type;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dateTime;

}
