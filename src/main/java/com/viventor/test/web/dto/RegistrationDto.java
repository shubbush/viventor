package com.viventor.test.web.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author shubanev.a
 */
@Builder
@Getter
@Setter
public class RegistrationDto {

    @NotNull
    @Email
    private String email;

    @NotNull
    @Size(min = 6)
    private String password;

}
