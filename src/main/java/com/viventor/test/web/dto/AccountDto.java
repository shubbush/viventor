package com.viventor.test.web.dto;

import lombok.*;

import java.math.BigDecimal;

/**
 * @author shubanev.a
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountDto {

    private Long id;
    private String currency;
    private BigDecimal balance;

}
