package com.viventor.test.web.controller;

import com.viventor.test.core.domain.Transaction;
import com.viventor.test.core.service.TransactionService;
import com.viventor.test.web.dto.TransactionDto;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author shubanev.a
 */
@RestController
@RequestMapping("/api/transaction")
public class TransactionController {

    private TransactionService transactionService;
    private ModelMapper mapper;

    @Autowired
    public TransactionController(TransactionService transactionService, ModelMapper mapper) {
        this.transactionService = transactionService;
        this.mapper = mapper;
    }

    @GetMapping("/{clientId}")
    public ResponseEntity<List<TransactionDto>> statement(@PathVariable("clientId") Long clientId,
                                                          @RequestParam("from")
                                                       @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime from,
                                                          @RequestParam("to")
                                                       @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") LocalDateTime to) {
        List<Transaction> transactions = transactionService.statement(clientId, from, to);
        Type listType = new TypeToken<List<TransactionDto>>() {}.getType();
        return ResponseEntity.ok(mapper.map(transactions, listType));
    }
}
