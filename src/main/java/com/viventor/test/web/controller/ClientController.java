package com.viventor.test.web.controller;

import com.viventor.test.core.domain.Client;
import com.viventor.test.web.dto.ClientDto;
import com.viventor.test.web.dto.RegistrationDto;
import com.viventor.test.core.service.ClientService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

/**
 * @author shubanev.a
 */
@RestController
@RequestMapping("/api/client")
public class ClientController {

    private ClientService clientService;
    private ModelMapper mapper;

    @Autowired
    public ClientController(ClientService clientService, ModelMapper mapper) {
        this.clientService = clientService;
        this.mapper = mapper;
    }

    @PostMapping("/register")
    public ResponseEntity register(@RequestBody @Valid RegistrationDto dto) {
        Client client = clientService.create(mapper.map(dto, Client.class));

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/../{id}")
                .buildAndExpand(client.getId())
                .normalize()
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<ClientDto> find(@PathVariable("id") Long id) {
        return ResponseEntity.ok(mapper.map(clientService.find(id), ClientDto.class));
    }


}
