package com.viventor.test.web.controller;

import com.viventor.test.core.domain.Account;
import com.viventor.test.core.service.AccountService;
import com.viventor.test.web.dto.AccountDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * @author shubanev.a
 */
@RestController
@RequestMapping("/api/account")
public class AccountController {

    private AccountService accountService;
    private ModelMapper mapper;

    @Autowired
    public AccountController(AccountService accountService, ModelMapper mapper) {
        this.accountService = accountService;
        this.mapper = mapper;
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<AccountDto> balance(@PathVariable("id") Long id) {
        Account account = accountService.find(id);
        return ResponseEntity.ok(mapper.map(account, AccountDto.class));
    }

    @GetMapping(path = "/{id}/withdraw")
    public ResponseEntity<AccountDto> withdraw(@PathVariable("id") Long id,
                                               @RequestParam("amount") BigDecimal amount) {
        Account account = accountService.withdraw(id, amount);
        return ResponseEntity.ok(mapper.map(account, AccountDto.class));
    }

    @GetMapping(path = "/{id}/deposit")
    public ResponseEntity<AccountDto> deposit(@PathVariable("id") Long id,
                                              @RequestParam("amount") BigDecimal amount) {
        Account account = accountService.deposit(id, amount);
        return ResponseEntity.ok(mapper.map(account, AccountDto.class));
    }



}
